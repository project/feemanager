<?php

/**
 * @file
 * Enables the privileged user to administer fee types and manage member fees.
 * Normal users who are paying fees can view their records.
 * Features: Multiple fee types for different types of users, ...
 */

/**
 * Implementation of hook_help().
 */
function feemanager_help($section) {
  switch ($section) {
    case 'admin/help#feemanager':
      $output = '<p>' . t('Click on menu item ' . l('feemanager', 'feemanager') . 
            ' to view/update user\'s ' .
      		'fee records and the fees that are due for users. <br/><br/>' .
      		'Click on menu item ' . l('administer/feemanager', 'admin/feemanager') . ' ' .
      		'to add/update fee types with their ' .
      		'respective amounts. You can also set a default there. You cannot set new fee types ' .
      		'to be the default yet, but this can be achieved by adding a new fee type and ' .
      		'clicking \'edit\' on the \'administer/feemanager\' page.') . '</p>';
      return $output;
    case 'admin/modules#description':
      return t('With the feemanager module privileged users can view/update user\'s ' .
      		'fee records. Privileged users can add and updated fee types ' .
      		'and set a default fee type for new users.');
  }
}

/**
 * Implementation of hook_perm().
 */
function feemanager_perm() {
  return array('administer fees', 'manage fees', 'view own fees');
}

/**
 * Implementation of hook_access().
 */
function feemanager_access($op) {
  if ($op == 'admin') {
    return user_access('administer fees');
  }
  if ($op == 'manage') {
    return user_access('manage fees');
  }
  if ($op == 'view') {
    user_access('view own fees');
  }
}

/**
 * Implementation of hook_menu().
 */
function feemanager_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'feemanager', // drupal path example.com/?q=feemanager or example.com/feemanager with clean URLs 
      'title' => 'feemanager', // page title
      'callback' => 'feemanager_page', // callback function name, function called when page is accessed
      'access' => user_access('manage fees'), // only fee managers can access this page
      'type' => MENU_NORMAL_ITEM, // define type of menu item as callback
    );
    $items[] = array(
      'path' => 'admin/feemanager',
      'title' => 'fee types',
      'callback' => 'feemanager_admin',
      'access' => user_access('administer fees'),
      'type' => MENU_NORMAL_ITEM,
    );
    $items[] = array(
      'path' => 'my fees',
      'title' => 'my fees',
      'callback' => 'feemanager_view',
      'access' => user_access('view own fees'),
      'type' => MENU_NORMAL_ITEM,
    );
    $items[] = array(
    	'path' => 'admin/feemanager/add',
      'title' => 'add fee type',
      'callback' => 'feemanager_add',
      'access' => user_access('administer fees'),
      'type' => MENU_NORMAL_ITEM,
    );
  }
  else {
  	if (arg(0) == 'admin' && arg(1) == 'feemanager' && is_numeric(arg(2))) {
  		$fee_types = feemanager_feeTypes();
	    $items[] = array(
          'path' => 'admin/feemanager/'. arg(2),
	      'title' => t("change '%fee_type'", array('%fee_type' => $fee_types[arg(2)]->type)),
	      'callback' => 'feemanager_admin_feetype_form',
	      'callback arguments' => array('feetype' => $fee_types[arg(2)]),
	      'type' => MENU_CALLBACK,
	      'access' => user_access('administer fees'),
	    );
  	}
  	if (arg(0) == 'admin' && arg(1) == 'feemanager' 
    && arg(2) == 'delete' && is_numeric(arg(3))) {
      $fee_types = feemanager_feeTypes();
      $items[] = array(
        'path' => 'admin/feemanager/delete/'. arg(3),
	    'title' => t("delete '%fee_type'", array('%fee_type' => $fee_types[arg(3)]->type)),
	    'callback' => 'feemanager_delete',
	    'callback arguments' => array('feetype' => $fee_types[arg(3)]),
	    'type' => MENU_CALLBACK,
	    'access' => user_access('administer fees'),
	  );
  	}
  	if (arg(0) == 'feemanager' && is_numeric(arg(1))) {
      $items[] = array(
        'path' => 'feemanager/' . arg(1),
        'title' => t('Update fee record for user with id ' . arg(1)),
        'callback' => 'feemanager_update_record',
        'callback arguments' => arg(1),
        'type' => MENU_CALLBACK,
        'access' => 'manage fees', 
      );
  	}
  }

  return $items;
}

/**
 * MENU_CALLBACK for menu item 'feemanager'
 * default page for feemanager
 */
function feemanager_page() {
  if (!user_access('manage fees')) {
    return message_access();
  }
		
  $default_fee_type = feemanager_default();
  $default_ft_string = $default_fee_type->type.' ($'.$default_fee_type->amount.')';
  $fee_types = feemanager_feeTypes();
  $row_ft = array();
	
  foreach ($fee_types as $name => $record) {
    $row_ft[$record->fid] = $record->type.' ($'.$record->amount.')';  
  }
	
  foreach (feemanager_userRecords() as $name => $record) {
    $form['users'][$record->name]['uid'] = array(
      '#value' =>	$record->uid,
    );
    $form['users'][$record->name]['name'] = array(
      '#value' => $record->name,
    );
    $form['users'][$record->name]['mail'] = array(
      '#value' => $record->mail,
    );
    $form['users'][$record->name]['fid'] = array(
      '#value' => $record->fid,
    );
    $form['users'][$record->name]['type'] = array(
      '#value' => isset($record->fid) ? $row_ft[$record->fid] : 'N/A',  
    );
    $form['users'][$record->name]['outstanding'] = array(
      '#value' => '$' . feemanager_getOutstanding($record->until, $record->uid),
    );
    $form['users'][$record->name]['until'] = array(
      '#value' => $record->until,
    );
	
    $form['users'][$record->name]['edit'] = array(
	'#value' => l(t('update'), 'feemanager/'. $record->uid),
	);
  }
	
  return drupal_get_form('feemanager_page', $form);
}

/**
 * MENU_CALLBACK for update $uid
 * privileged users update other user's fee record
 */
function feemanager_update_record($uid) {
  if (!user_access("manage fees")) {
    return message_access(); 
  }

  $output = '';
	
  if (is_numeric($uid)) {
    $row_ft =array();
    $fee_types = feemanager_feeTypes();
    foreach ($fee_types as $name => $record) {
      $row_ft[$record->fid] = $record->type . ' ($' . $record->amount . ')';
    }
    $record = feemanager_userRecord($uid);
    $user_feetype = $record->type . ' ($' . $record->amount . ')';

    $output .= '<p>'.t('update record for user').': <strong>'.$record->name.'</strong></p>';

    $form['feetype'] = array(
      '#type' => 'select',
      '#title' => t('Fee type'),
      '#default_value' => $record->fid,
      '#options' => $row_ft,
      '#description' => t('Select a fee type for this user.'),
    );
    $form['paid'] = array(
      '#type' => 'textfield',
      '#title' => 'paid until',
      '#default_value' => isset($record->until) ? $record->until : feemanager_today(),
      '#description' => t('Format: YYYY-MM-DD (e.g. '.feemanager_today().' for today\'s date)'),
    );
		
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Update record',
    );
		
  }
  
  return $output . drupal_get_form('feemanager_update_record', $form);
}

/**
 * Implementation of hook_submit().
 * happens after submit button 'Update record' is pressed and input is validated
 * with hook_validate
 */
function feemanager_update_record_submit($form_id, $form_values) {
  if (!is_numeric(arg(1))) {
    //return '';
    // error processing (if arguments are wrong)
  }

  $new_paid_until_date = $form_values['paid'];
  $result = db_query('SELECT * FROM {fee_pay} where uid=%d',	arg(1));

  $record = db_fetch_object($result);

  // if the user already has a fee type
  if (is_numeric($record->fid)) {
    db_query('UPDATE {fee_pay} SET until="%s" WHERE uid=%d', $new_paid_until_date, $record->uid);
    db_query('UPDATE {fee_pay} SET fid=%d WHERE uid=%d', $form_values['feetype'], $record->uid);		
  }
  else {
    // fid = 1 is the default value for fee type		
    db_query(
        'INSERT INTO {fee_pay} (uid, fid, until) ' .
        'VALUES (%d, %d, "%s")', arg(1), $form_values['feetype'], 
        $form_values['paid']);
  }

  return 'feemanager';
}

/**
 * Implementation of hook_validate().
 * validate input for update_record
 */
function feemanager_update_record_validate($form_id, $form_values) {
  $paid_until_date = $form_values['paid'];
  $valid_date_string = '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/'; // matches 'YYYY-MM-DD' where Y,M,D are digits (0-9)
  if ($paid_until_date == '') {
    form_set_error('', t('You must provide a date until when the user has paid.'));
  }
  elseif (!preg_match($valid_date_string, $paid_until_date)) {
    form_set_error('', t($paid_until_date.' is not in the right format (YYYY-MM-DD).'));
  }
  else {
    $date_array = explode('-', $paid_until_date);
    $year = (int) $date_array[0];
    $month = (int) $date_array[1];
    $day = (int) $date_array[2];
    if (!($day > 0 && $day <= 31						// 0 < day <= 31  
      && $month > 0 && $month <= 12 			// 0 < month <= 12
      && $year >= 1900 && $year <= 2100)) {	// 1900 <= year <= 2100
        form_set_error('', t($paid_until_date.
            ' is not in the right format ' .
            '(in YYYY-MM-DD values must be: ' .
            '01 <= DD <= 31, 01 <= MM <= 12, 1900 <= YYYY <= 2100.'));
    }
  }
}

/**
 * MENU_CALLBACK for menu item 'my fees'
 * my+fees page - users view their own fee record
 */
function feemanager_view() {
  if (!user_access("view own fees")) {
    return message_access(); 
  }

  global $user;
  
  $fee_record = feemanager_userRecord($user->uid);
  
  $rows = array();
  
  $rows[] = array(
      'Your fee type:', 
      $fee_record->type  . ' ($' . $fee_record->amount . ')'
  );
  $rows[] = array(
      'You paid until:',
      $fee_record->until
  );
  $rows[] = array(
      'Outstanding balance:', 
      '$' . feemanager_getMonths($fee_record->until) * $fee_record->amount
  );

  $html = theme_table(null, $rows);

  return $html;
}

/**
 * MENU_CALLBACK for menu item administer/feemanager
 * admin/feemanager page - administer fee types
 */
function feemanager_admin() {
  // only administrators can access this part of the module
  if (!user_access("administer fees")) {
    return message_access(); 
  }
  
  $feetypes = feemanager_feeTypes();

  foreach ($feetypes as $key => $feetype) {
    $options[$key] = '';

    $form['fee_types'][$feetype->type]['fid'] = array(
      '#value' => $feetype->fid,
      '#type' => 'hidden',
    );

    $form['fee_types'][$feetype->type]['default'] = array(
      '#value' => '*',
    );
    $form['fee_types'][$feetype->type]['type'] = array(
      '#value' => $feetype->type
    );
    $form['fee_types'][$feetype->type]['amount'] = array(
      '#value' => $feetype->amount
    );
    $form['fee_types'][$feetype->type]['edit'] = array(
      '#value' => l(t('edit'), 'admin/feemanager/'. $feetype->fid)
    );
    $form['fee_types'][$feetype->type]['delete'] = array(
      '#value' => l(t('delete'), 'admin/feemanager/delete/'. $feetype->fid),
    );
  }

  $output = '<br/>' . l('Add new fee type', 'admin/feemanager/add');

  return drupal_get_form('feemanager_admin', $form) . $output;
}

/**
 * MENU_CALLBACK for 'admin/feemanager/add'
 * delete fee type with $fid
 */
function feemanager_add() {
  $form['type'] = array(
    '#type' => 'textfield', 
    '#title' => t('Fee type'),
    '#description' => t("A short textual description of the new fee type."), 
    '#maxlength' => '30', '#size' => '15'
  );
  $form['amount'] = array(
    '#type' => 'textfield', 
    '#title' => t('Amount ($)'),
    '#description' => t("An amount in the form 'X.Y' where X, Y are natural numbers (0,1,2,3,...)."), 
    '#maxlength' => '11', '#size' => '11'
  );
  $form['default'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Default'),
    '#description' => t('If ticked, this fee type becomes the default type, meaning it is automatically set for new users.'),		
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Add new fee type',
  );

  return drupal_get_form('feemanager_add', $form); 
}

function feemanager_add_validate($form_id, $form_values) {
  $validFeeTypeRegex = '/^[A-Za-z0-9,. ]+$/';
  
  $log = array();
  $error = false;
  
  $amount = $form_values['amount'];
  $feeType = $form_values['type'];
  
  $validAmount = feemanager_validAmount($amount); 
  $validFeeType = preg_match($validFeeTypeRegex, $feeType);
           
  if (!$validFeeType) {
    $log[] = 'Fee type: "' . $feeType . '" should not contain other ' .
        'characters than alphanumeric or "," and ".".';
    $error = true;
  }
  if (!$validAmount) {
    $log[] = 'Amount: "' . $amount . '" should be a positive ' .
        'number (e.g. 0.10, 1, 2, 3.00, 4.00, 5.78)';
    $error = true;
  }
  
  if ($error) {
    $logMessages = implode('<br/><br/>', $log);
    form_set_error('', t($logMessages));
  }
  //return $valid;
}

/**
 * Implementation of hook_submit().
 * happens after submit button 'Add new fee type' is pressed and input is validated
 * with hook_validate
 */
function feemanager_add_submit($form_id, $form_values) {
  $addFeeTypeQuery = 
      'INSERT INTO {fee_type} (fid, type, amount) ' .
      'VALUES (%d, "%s", %d)';
  $newFid = feemanager_getNewFid();
  db_query('START TRANSACTION');
  db_query($addFeeTypeQuery, $newFid, 
           $form_values['type'], $form_values['amount']);
    
  if ($form_values['default']) {
    feemanager_setDefaultFid($newFid); 
  }
  db_query('COMMIT');

  return 'admin/feemanager';
}

/**
 * MENU_CALLBACK for 'admin/feemanager/delete/$fid'
 * delete fee type with $fid
 */
function feemanager_delete() {
  $to_delete_fid = arg(3);
  // we don't want to delete the default fee type'
  if ($to_delete_fid > 1) {
    // todo: start a transaction
    // we need to make sure that fee_pay records have a valid
    // (default) fid
    db_query('UPDATE {fee_pay} SET fid=1 WHERE fid=%d', $to_delete_fid);
    db_query('DELETE FROM {fee_type} WHERE fid=%d', $to_delete_fid);
  }

  $output = '<p>Deleted fee type ' . $to_delete_fid . '</p>' . 
      l('Back to fee types', 'admin/feemanager');

  return $output;
}

/**
 * Implementation of theme_hook().
 */
function theme_feemanager_admin($form) {
  $rows = array();
  $i = 0;
  foreach ($form['fee_types'] as $name => $element) {
    if (isset($element['type'])) {
      $i++;
      $rows[] = array(
        ($i == 1) ? '*' : '',
        form_render($element['type']),
        form_render($element['amount']),
        form_render($element['edit']),
        form_render($element['delete']),
      );
      unset($form['fee_types'][$name]);
    }
  }
  $header = array(
    t('Default'), 
    t('Type'), 
    t('Amount'), 
    array('data' => t('Operations'), 'colspan' => 2),
  );
  $output = theme('table', $header, $rows);
  $output .= form_render($form);

  return $output;
}

/**
 * Implementation of theme_hook().
 */
function theme_feemanager_page($form) {
  $rows = array();
  foreach ($form['users'] as $name => $record) {
    if (isset($record['name'])) {
      $rows[] = array(
        form_render($record['name']),
        form_render($record['mail']),
        form_render($record['type']),
        form_render($record['until']),				
        form_render($record['outstanding']),
        form_render($record['edit']),				
      );
      unset($form['users'][$name]);
    }
  }
  $header = array(
    t('Username'), 
    t('Email'), 
    t('Fee type'), 
    t('Paid until'),
    t('Due'), 
    t('Operation') // operations to add: paid until today, ...
  );
  $output = theme('table', $header, $rows);
  $output .= form_render($form);

  return $output;
}

/**
 * MENU_CALLBACK for 'admin/feemanager/$fid'
 * administer fee type with $fid
 */
function feemanager_admin_feetype_form() {
  $fid = arg(2);

  $fee_record = feemanager_fee_type($fid);

  $form['fid'] = array(
    '#type' => 'hidden',
    '#value' => $fid,
  );
  $form['type'] = array(
    '#type' => 'textfield',
    '#title' => 'Type',
    '#default_value' => $fee_record->type,
    '#size' => 30,
    '#maxlength' => 30,
    '#required' => TRUE,		
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => 'Amount',
    '#default_value' => $fee_record->amount,
    '#size' => 11,
    '#maxlength' => 11,
    '#required' => TRUE,		
  );
  $form['default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default'),
    '#default_value' => ($fee_record->fid == 1),		
  );
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Change fee type'),
  );

  return drupal_get_form('feemanager_admin_feetype_form', $form);
}

/**
 * Implementation of hook_submit().
 * happens after submit button 'Change fee type' is pressed and 
 * todo: input is validated with hook_validate
 */
function feemanager_admin_feetype_form_submit($form_id, $form_values) {
  $currentType = $form_values['type'];
  $currentAmount = $form_values['amount'];
  $currentDefault = $form_values['default'];
  $currentFid = $form_values['fid'];

  $updateFeeTypeQuery = 
      'UPDATE {fee_type} SET type="%s", amount=%d ' .
      'WHERE fid=%d';

  db_query('START TRANSACTION');
  db_query($updateFeeTypeQuery, $currentType, $currentAmount, $currentFid);
  
  // if default is selected
  if ($currentDefault) {
    feemanager_setDefaultFid($currentFid);
  }
  db_query('COMMIT');

  return 'admin/feemanager';
}

/**
 * Implementation of hook_submit().
 */
function feemanager_admin_submit($form_id, $form_values) {
  return $form_values['type'];
}

/**
 * get all fee types ordered by fid, so the first is the default
 * @return array associative with fid as key
 */
function feemanager_feeTypes() {
  $fee_types = array();
  $result = db_query('SELECT fid, type, amount FROM {fee_type} ORDER BY fid'); 
  while ($fee_type = db_fetch_object($result)) {
    $fee_types[$fee_type->fid] = $fee_type;
  }
  return $fee_types;
}

/**
 * get fee type data of fee type with $fid
 * @param $fid a valid fee id
 * @return object $object->fid, $object->type, $object->amount
 */
function feemanager_fee_type($fid) {
  $result = db_query('SELECT fid, type, amount FROM {fee_type} WHERE fid=%d', $fid);
  return db_fetch_object($result);	
}

/**
 * get the default fee type
 * @return object $object->type, $object->amount
 */
function feemanager_default() {
  $result = db_query('SELECT type, amount FROM {fee_type} WHERE fid=1');
  $first = db_fetch_object($result);
  return $first;
}

/**
 * get all user records with their respective fee data
 * into an associative array of objects
 * @return array associative with user name as key
 */
function feemanager_userRecords() {
  $users = array();
  $result = db_query(
      'SELECT u.uid, u.name, u.mail, ft.fid, ft.type, ft.amount, fp.until ' .	
      'FROM {users} u ' .
      'LEFT JOIN {fee_pay} fp ON u.uid=fp.uid ' .
      'LEFT JOIN {fee_type} ft ON fp.fid=ft.fid ' .
      'WHERE u.uid>0');
  while ($user = db_fetch_object($result)) {
    $users[$user->name] = $user;
  }
  return $users;
}

/**
 * get the complete fee record for user with $uid
 * @return object with $object->uid, $object->name, $object->mail, $object->fid, $object->type, $object->amount, $object->until  
 */
function feemanager_userRecord($uid) {
  $result = db_query(
      'SELECT u.uid, u.name, u.mail, ft.fid, ft.type, ft.amount, fp.until ' .	
      'FROM {users} u ' .
      'LEFT JOIN {fee_pay} fp ON u.uid=fp.uid ' .
      'LEFT JOIN {fee_type} ft ON fp.fid=ft.fid ' .
      'WHERE u.uid=%d', $uid);
  return db_fetch_object($result);
}

/**
 * get today's date in YYYY-MM-DD format
 * @return date
 */
function feemanager_today() {
  return date("Y-m-d");
}

/**
 * get the number of months between today and $date
 * @param $date to calculate difference from
 * @return int number of months between now and $date
 */
function feemanager_getMonths($date) {
  $today = feemanager_today();
  $today_array = explode('-', $today);
  $date_array  = explode('-', $date);

  $today_year = (int) $today_array[0]; 
  $today_month = (int) $today_array[1]; 
  $today_day = (int) $today_array[2]; 

  $date_year = (int) $date_array[0];
  $date_month = (int) $date_array[1];
  $date_day = (int) $date_array[2];

  $months = 0;
  if ($date_day < $today_day) {
    $months++;
  }
  $months += ($today_year - $date_year) * 12 + ($today_month - $date_month);

  return $months;
}

/**
 * get the outstanding amount of money a user has to pay
 * @param string $date of form 'YYYY-MM-DD'
 * @param int $uid user ID
 * @return decimal X.YY where X is an integer and Y a digit
 */
function feemanager_getOutstanding($date, $uid) {
  $record = feemanager_userRecord($uid);
  return feemanager_getMonths($record->until) * $record->amount;
}

function feemanager_getMaxFid() {
  $result = db_query('SELECT MAX(fid) as max FROM {fee_type}');     
  $maxFid = db_fetch_object($result)->max;
  return $maxFid;
}

function feemanager_getNewFid() {
  $newFid = feemanager_getMaxFid() + 1;
  return $newFid;
}

/**
 * set fee type with $fid to default fee type
 * THIS METHOD SHOULD BE PART OF A TRANSACTION
 * @param int $fid fee id
 * @return boolean TRUE if database is changed false otherwise
 */
function feemanager_setDefaultFid($fid) {
  // if this is already the default
  if ($fid == 1) {
    // do not change fid
    //return FALSE;
    echo 'default';
  }
  else { // things get complicated :-)
    //echo 'not default';
    $updateFeeTypeQuery = 'UPDATE {fee_type} SET fid=%d WHERE fid=%d';
    $updateFeePayQuery  = 'UPDATE {fee_pay} SET fid=%d WHERE fid=%d';
  	// start a transaction
  	//db_query('START TRANSACTION');
  	// get the max(fid)+1 of the fee type table
    $newFid = feemanager_getNewFid();
  	// update fee type with fid=1 to new fid
  	db_query($updateFeeTypeQuery, $newFid, 1);
  	// update all fee_pay records to new fid where fid=1
  	db_query($updateFeePayQuery, $newFid, 1);
    // update fee type with $fid to $fid=1
    db_query($updateFeeTypeQuery, 1, $fid);
  	// update all fee_pay records with $fid to fid=1
  	db_query($updateFeePayQuery, 1, $fid);
  	//db_query('COMMIT');
    //return TRUE;
  }
}

function feemanager_validAmount($amount) {
  $amount = (string) $amount;
  $intRegex = '/^[0-9]+$/';
  if (preg_match($intRegex, $amount)
      && ((int) $amount > 0)) {
    return TRUE;
  }
  $amountArray = explode('.', $amount);
  $dollars = (int) $amountArray[0];
  $cents = $amountArray[1];
  if (!((int) $cents >= 0 && (int) $cents < 100
      && $dollars >= 0 && preg_match('/^[0-9]{1,2}$/', $cents)
      && ($dollars > 0 || (int) $cents > 0))) {
        return FALSE;
  }
  else {
    // regular expressions are slow and hard to write so omitted the following
    //$validAmount = '/^[0-9]{0,9999}-[0-9]{2}-[0-9]{2}$/';
    return TRUE;
  }
}